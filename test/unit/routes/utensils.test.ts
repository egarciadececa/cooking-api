import request from "supertest";
import { mocked } from "jest-mock";

import { Step, Utensil } from "@/services/db";
import {
  IngredientType,
  Utensil as UtensilType,
  Step as StepType,
} from "@/services/schemas";

import app from "@/app";

jest.mock("@/services/db/utensil");
jest.mock("@/services/db/step");

const mockedUtensil = mocked(Utensil);
const mockedStep = mocked(Step);

describe("The /utensils route", () => {
  describe("GET /utensils", () => {
    it("Returns 200 and an array of utensils", async () => {
      // given
      mockedUtensil.getAll.mockResolvedValueOnce([
        {
          id: 1,
          name: "utensil-1",
          waitTimeInMillis: 1000,
        },
        {
          id: 2,
          name: "utensil-2",
          waitTimeInMillis: 2000,
        },
        {
          id: 3,
          name: "utensil-1",
          waitTimeInMillis: 3000,
        },
        {
          id: 4,
          name: "utensil-2",
          waitTimeInMillis: 4000,
        },
      ]);

      // when
      const response = await request(app).get("/utensils");

      // then
      expect(response.status).toBe(200);
      expect(response.body).toBeDefined();
      expect(response.body).toBeInstanceOf(Array);

      response.body.forEach((utensil: UtensilType) => {
        expect(utensil).toMatchObject({
          id: expect.any(Number),
          name: expect.any(String),
          waitTimeInMillis: expect.any(Number),
        });
      });
    });
  });

  describe("POST /utensils", () => {
    it.each`
      body                          | message                             | reason
      ${undefined}                  | ${/is required/}                    | ${"is undefined"}
      ${{}}                         | ${/is required/}                    | ${"is empty"}
      ${{ waitTimeInMillis: 1000 }} | ${/"name" is required/}             | ${"has no name field"}
      ${{ name: "utensil-1" }}      | ${/"waitTimeInMillis" is required/} | ${"has no waitTimeInMillis field"}
    `("Returns 400 if the body $reason", async ({ body, message }) => {
      // given

      // when
      const response = await request(app).post("/utensils").send(body);

      // then
      expect(response.status).toEqual(400);
      expect(response.body).toBeDefined();
      expect(response.body.message).toMatch(message);
    });

    it("Returns 200 and the created utensil", async () => {
      // given
      const body = {
        name: "utensil-1",
        waitTimeInMillis: 1000,
      };

      mockedUtensil.create.mockResolvedValueOnce(1);
      mockedUtensil.get.mockResolvedValueOnce({ id: 1, ...body });

      // when
      const response = await request(app).post("/utensils").send(body);

      // then
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty("id");
      expect(response.body).toMatchObject(body);
    });
  });

  describe("GET /utensils/{id}", () => {
    it("Returns 400 if the id is not a number", async () => {
      // given
      const ID = "INVALID";

      // when
      const response = await request(app).get(`/utensils/${ID}`);
      // then
      expect(response.status).toEqual(404);
      expect(response.body).toBeDefined();
      expect(response.body.message).toBe("Endpoint not found");
    });

    it("Returns 400 if the utensil doesn't exist", async () => {
      // given
      const ID = 101;

      mockedUtensil.get.mockResolvedValueOnce(undefined);

      // when
      const response = await request(app).get(`/utensils/${ID}`);
      // then
      expect(response.status).toEqual(404);
      expect(response.body).toBeDefined();
      expect(response.body.message).toBe("Utensil not found");
    });

    it("Returns 200 and the utensil if the id is valid and exists", async () => {
      // given
      const ID = 101;

      const utensil = {
        id: ID,
        name: "utensil-name",
        waitTimeInMillis: 1000,
      };
      mockedUtensil.get.mockResolvedValueOnce(utensil);

      // when
      const response = await request(app).get(`/utensils/${ID}`);

      // then
      expect(response.status).toBe(200);
      expect(response.body).toBeDefined();
      expect(response.body).toEqual(utensil);
    });
  });

  describe("PUT /utensils/{id}", () => {
    it("Returns 400 if the id is not a number", async () => {
      // given
      const ID = "INVALID";
      const body = {};

      // when
      const response = await request(app).put(`/utensils/${ID}`).send(body);

      // then
      expect(response.status).toEqual(404);
      expect(response.body).toBeDefined();
      expect(response.body.message).toBe("Endpoint not found");
    });

    it("Returns 400 if the utensil doesn't exist", async () => {
      // given
      const ID = 1;
      const body = {};

      mockedUtensil.get.mockResolvedValueOnce(undefined);

      // when
      const response = await request(app).put(`/utensils/${ID}`).send(body);

      // then
      expect(response.status).toEqual(404);
      expect(response.body).toBeDefined();
      expect(response.body.message).toBe("Utensil not found");
    });

    it.each`
      body                          | message                             | reason
      ${undefined}                  | ${/is required/}                    | ${"is undefined"}
      ${{}}                         | ${/is required/}                    | ${"is empty"}
      ${{ waitTimeInMillis: 1000 }} | ${/"name" is required/}             | ${"has no name field"}
      ${{ name: "name-updated" }}   | ${/"waitTimeInMillis" is required/} | ${"has no waitTimeInMillis field"}
    `("Returns 400 if the body $reason", async ({ body, message }) => {
      // given
      const ID = 1;

      mockedUtensil.get.mockResolvedValueOnce({
        id: ID,
        name: "utensil-name",
        waitTimeInMillis: 1000,
      });

      // when
      const response = await request(app).put(`/utensils/${ID}`).send(body);

      // then
      expect(response.status).toEqual(400);
      expect(response.body).toBeDefined();
      expect(response.body.message).toMatch(message);
    });

    it("Returns 200 and the updated utensil if the id is valid and exists, and the body is valid", async () => {
      // given
      const ID = 1;
      const body = { name: "name-updated", waitTimeInMillis: 1500 };

      mockedUtensil.get.mockResolvedValueOnce({
        id: ID,
        name: "utensil-name",
        waitTimeInMillis: 1000,
      });
      mockedUtensil.update.mockResolvedValueOnce(ID);
      mockedUtensil.get.mockResolvedValueOnce({
        id: ID,
        ...body,
      });

      // when
      const response = await request(app).put(`/utensils/${ID}`).send(body);

      // then
      expect(response.status).toBe(200);
      expect(response.body).toBeDefined();
      expect(response.body).toStrictEqual({
        id: ID,
        ...body,
      });
    });
  });

  describe("DELETE /utensils/{id}", () => {
    it("Returns 400 if the id is not a number", async () => {
      // given
      const ID = "INVALID";

      // when
      const response = await request(app).delete(`/utensils/${ID}`);
      // then
      expect(response.status).toEqual(404);
      expect(response.body).toBeDefined();
      expect(response.body.message).toBe("Endpoint not found");
    });

    it("Returns 400 if the utensil doesn't exist", async () => {
      // given
      const ID = 101;

      mockedUtensil.get.mockResolvedValueOnce(undefined);

      // when
      const response = await request(app).delete(`/utensils/${ID}`);
      // then
      expect(response.status).toEqual(404);
      expect(response.body).toBeDefined();
      expect(response.body.message).toBe("Utensil not found");
    });

    it("Returns 204 after deleting the utensil", async () => {
      // given
      const ID = 1;

      mockedUtensil.get.mockResolvedValueOnce({
        id: ID,
        name: "utensil-name",
        waitTimeInMillis: 1000,
      });
      mockedStep.search.mockResolvedValueOnce([]);
      mockedUtensil.destroy.mockResolvedValueOnce(1);

      // when
      const response = await request(app).delete(`/utensils/${ID}`);

      // then
      expect(response.status).toBe(204);
      expect(response.body).toStrictEqual({});

      expect(await Utensil.get(ID)).toBeUndefined();
    });

    it("Returns 400 if the utensil is being used in a step", async () => {
      // given
      const ID = 1;
      const step = {
        input: 101,
        utensil: ID,
        output: 102,
      };

      mockedUtensil.get.mockResolvedValueOnce({
        id: ID,
        name: "utensil-name",
        waitTimeInMillis: 1000,
      });
      mockedStep.search.mockResolvedValueOnce([step]);

      // when
      const response = await request(app).delete(`/utensils/${ID}`);

      // then
      expect(response.status).toBe(400);
      expect(response.body).toBeDefined();
      expect(response.body.message).toBe(
        "The utensil is being used on steps. It can't be deleted."
      );
      expect(response.body.steps).toMatchObject([step]);
    });
  });

  describe("Queries on steps", () => {
    describe("GET /utensils/{id}/uses", () => {
      it("Returns 400 if the id is not a number", async () => {
        // given
        const ID = "INVALID";

        // when
        const response = await request(app).get(`/utensils/${ID}/uses`);
        // then
        expect(response.status).toEqual(404);
        expect(response.body).toBeDefined();
        expect(response.body.message).toBe("Endpoint not found");
      });

      it("Returns 400 if the ingredient doesn't exist", async () => {
        // given
        const ID = 9999;

        mockedUtensil.get.mockResolvedValueOnce(undefined);

        // when
        const response = await request(app).get(`/utensils/${ID}/uses`);

        // then
        expect(response.status).toEqual(404);
        expect(response.body).toBeDefined();
        expect(response.body.message).toBe("Utensil not found");
      });

      it("Returns 200 and the list of detailed steps that use the utensil", async () => {
        // given
        const ID = 1;
        mockedUtensil.get.mockResolvedValueOnce({
          id: ID,
          name: "utensil-name",
          waitTimeInMillis: 1000,
        });
        mockedStep.searchDetailed.mockResolvedValueOnce([
          {
            input: {
              id: 101,
              name: "start",
              type: IngredientType.START,
            },
            utensil: {
              id: ID,
              name: "utensil-1",
              waitTimeInMillis: 100,
            },
            output: {
              id: 102,
              name: "mid",
              type: IngredientType.MID,
            },
          },
        ]);

        // when
        const response = await request(app).get(`/utensils/${ID}/uses`);

        // then
        expect(response.status).toBe(200);
        expect(response.body).toBeDefined();
        expect(response.body).toBeInstanceOf(Array);

        response.body.forEach((step: StepType) => {
          expect(step).toMatchObject({
            input: expect.any(Object),
            utensil: expect.any(Object),
            output: expect.any(Object),
          });
          expect(step.utensil).toMatchObject({ id: ID });
        });
      });
    });
  });
});
