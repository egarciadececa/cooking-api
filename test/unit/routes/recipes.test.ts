import request from "supertest";
import { mocked } from "jest-mock";

import { Recipe } from "@/services/db";
import { IngredientType, Recipe as RecipeType } from "@/services/schemas";
import app from "@/app";

jest.mock("@/services/db/recipe");

const mockedRecipe = mocked(Recipe);

describe("The /recipes route", () => {
  describe("GET /recipes", () => {
    it.each`
      query                          | condition
      ${"?detailed=whatIsThisValue"} | ${"the detail parameter has an invalid value"}
      ${"?detailed"}                 | ${"the detail parameter has no value"}
    `("Returns 400 if $condition", async ({ query }) => {
      // given

      // when
      const response = await request(app).get("/recipes" + query);

      // then
      expect(response.status).toBe(400);
      expect(response.body).toBeDefined();
      expect(response.body.message).toEqual('"detailed" must be a boolean');
    });

    it.each`
      query                | condition
      ${""}                | ${"the detailed parameter is not present"}
      ${"?detailed=false"} | ${"'detailed=false'"}
    `(
      "Returns 200 and an array of simple steps if $condition",
      async ({ query }) => {
        // given
        mockedRecipe.getAll.mockResolvedValueOnce([
          {
            input: 101,
            utensil1: 1,
            mid1: 102,
            utensil2: 2,
            mid2: 103,

            output: 103,
            steps: 2,
          },
          {
            input: 101,
            utensil1: 1,
            mid1: 102,
            utensil2: 3,
            mid2: 104,

            output: 104,
            steps: 2,
          },
        ]);

        // when
        const response = await request(app).get("/recipes" + query);

        // then
        expect(response.status).toBe(200);
        expect(response.body).toBeDefined();
        expect(response.body).toBeInstanceOf(Array);

        response.body.forEach((recipe: RecipeType) => {
          expect(recipe).toMatchObject({
            input: expect.any(Number),
            utensil1: expect.any(Number),
            mid1: expect.any(Number),

            output: expect.any(Number),
            steps: expect.any(Number),
          });
        });
      }
    );

    it("Returns 200 and an array of detailed steps if 'detailed=true'", async () => {
      // given
      mockedRecipe.getAllDetailed.mockResolvedValueOnce([
        {
          input: {
            id: 101,
            name: "start",
            type: IngredientType.START,
          },
          utensil1: {
            id: 1,
            name: "utensil-1",
            waitTimeInMillis: 100,
          },
          mid1: {
            id: 102,
            name: "mid",
            type: IngredientType.MID,
          },
          utensil2: {
            id: 2,
            name: "utensil-2",
            waitTimeInMillis: 200,
          },
          mid2: {
            id: 103,
            name: "end-1",
            type: IngredientType.END,
          },

          output: {
            id: 103,
            name: "end-1",
            type: IngredientType.END,
          },
          steps: 2,
        },
        {
          input: {
            id: 101,
            name: "start",
            type: IngredientType.START,
          },
          utensil1: {
            id: 1,
            name: "utensil-1",
            waitTimeInMillis: 100,
          },
          mid1: {
            id: 102,
            name: "mid",
            type: IngredientType.MID,
          },
          utensil2: {
            id: 3,
            name: "utensil-3",
            waitTimeInMillis: 300,
          },
          mid2: {
            id: 104,
            name: "end-2",
            type: IngredientType.END,
          },

          output: {
            id: 104,
            name: "end-2",
            type: IngredientType.END,
          },
          steps: 2,
        },
      ]);

      // when
      const response = await request(app).get("/recipes?detailed=true");

      // then
      expect(response.status).toBe(200);
      expect(response.body).toBeDefined();
      expect(response.body).toBeInstanceOf(Array);

      response.body.forEach((recipe: RecipeType) => {
        expect(recipe).toMatchObject({
          input: expect.any(Object),
          utensil1: expect.any(Object),
          mid1: expect.any(Object),

          output: expect.any(Object),
          steps: expect.any(Number),
        });
      });
    });
  });
});
