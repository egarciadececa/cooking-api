import request from "supertest";

import { environment } from "@/config/index";

import app from "@/app";

jest.mock("@/services/db", () => ({
  Ingredient: { count: jest.fn(() => 22) },
  Utensil: { count: jest.fn(() => 6) },
  Step: { count: jest.fn(() => 19) },
  Recipe: { count: jest.fn(() => 10) },
}));

describe("The / route", () => {
  describe("GET /", () => {
    it("Returns 200, the environment and DB stats", async () => {
      // given

      // when
      const response = await request(app).get("/");

      // then
      expect(response.status).toBe(200);
      expect(response.body).toBeDefined();
      expect(response.body.environment).toEqual(environment);
      expect(response.body.stats).toMatchObject({
        ingredients: expect.any(Number),
        utensils: expect.any(Number),
        steps: expect.any(Number),
        recipes: expect.any(Number),
      });
    });
  });
});
