import request from "supertest";
import { mocked } from "jest-mock";

import { Ingredient, Step } from "@/services/db";
import { IngredientType, Step as StepType } from "@/services/schemas";

import app from "@/app";

jest.mock("@/services/db/ingredient");
jest.mock("@/services/db/step");

const mockedIngredient = mocked(Ingredient);
const mockedStep = mocked(Step);

describe("The /ingredients route", () => {
  describe("GET /ingredients", () => {
    it("Returns 200 and an array of ingredients", async () => {
      // given
      mockedIngredient.getAll.mockResolvedValueOnce([
        {
          id: 101,
          name: "start",
          type: IngredientType.START,
        },
        {
          id: 102,
          name: "mid",
          type: IngredientType.MID,
        },
        {
          id: 103,
          name: "end-1",
          type: IngredientType.END,
        },
        {
          id: 104,
          name: "end-2",
          type: IngredientType.END,
        },
      ]);

      // when
      const response = await request(app).get("/ingredients");

      // then
      expect(response.status).toBe(200);
      expect(response.body).toBeDefined();
      expect(response.body).toBeInstanceOf(Array);

      response.body.forEach((ingredient: IngredientType) => {
        expect(ingredient).toMatchObject({
          id: expect.any(Number),
          name: expect.any(String),
          type: expect.any(String),
        });
      });
    });
  });

  describe("POST /ingredients", () => {
    it.each`
      body                              | message                 | reason
      ${undefined}                      | ${/is required/}        | ${"is undefined"}
      ${{}}                             | ${/is required/}        | ${"is empty"}
      ${{ type: IngredientType.START }} | ${/"name" is required/} | ${"has no name field"}
      ${{ name: "ing-1" }}              | ${/"type" is required/} | ${"has no type field"}
    `("Returns 400 if the body $reason", async ({ body, message }) => {
      // given

      // when
      const response = await request(app).post("/ingredients").send(body);

      // then
      expect(response.status).toEqual(400);
      expect(response.body).toBeDefined();
      expect(response.body.message).toMatch(message);
    });

    it("Returns 200 and the created ingredient", async () => {
      // given
      const body = {
        name: "ingredient-1",
        type: IngredientType.START,
      };

      mockedIngredient.create.mockResolvedValueOnce(101);
      mockedIngredient.get.mockResolvedValueOnce({ id: 101, ...body });

      // when
      const response = await request(app).post("/ingredients").send(body);

      // then
      expect(response.status).toBe(200);
      expect(response.body).toHaveProperty("id");
      expect(response.body).toMatchObject(body);
    });
  });

  describe("GET /ingredients/{id}", () => {
    it("Returns 400 if the id is not a number", async () => {
      // given
      const ID = "INVALID";

      // when
      const response = await request(app).get(`/ingredients/${ID}`);
      // then
      expect(response.status).toEqual(404);
      expect(response.body).toBeDefined();
      expect(response.body.message).toBe("Endpoint not found");
    });

    it("Returns 400 if the ingredient doesn't exist", async () => {
      // given
      const ID = 101;

      mockedIngredient.get.mockResolvedValueOnce(undefined);

      // when
      const response = await request(app).get(`/ingredients/${ID}`);
      // then
      expect(response.status).toEqual(404);
      expect(response.body).toBeDefined();
      expect(response.body.message).toBe("Ingredient not found");
    });

    it("Returns 200 and the ingredient if the id is valid and exists", async () => {
      // given
      const ID = 101;

      const ingredient = {
        id: ID,
        name: "ingredient-name",
        type: IngredientType.START,
      };
      mockedIngredient.get.mockResolvedValueOnce(ingredient);

      // when
      const response = await request(app).get(`/ingredients/${ID}`);

      // then
      expect(response.status).toBe(200);
      expect(response.body).toBeDefined();
      expect(response.body).toEqual(ingredient);
    });
  });

  describe("PUT /ingredients/{id}", () => {
    it("Returns 400 if the id is not a number", async () => {
      // given
      const ID = "INVALID";
      const body = {};

      // when
      const response = await request(app).put(`/ingredients/${ID}`).send(body);

      // then
      expect(response.status).toEqual(404);
      expect(response.body).toBeDefined();
      expect(response.body.message).toBe("Endpoint not found");
    });

    it("Returns 400 if the ingredient doesn't exist", async () => {
      // given
      const ID = 9999;
      const body = {};

      mockedIngredient.get.mockResolvedValueOnce(undefined);

      // when
      const response = await request(app).put(`/ingredients/${ID}`).send(body);

      // then
      expect(response.status).toEqual(404);
      expect(response.body).toBeDefined();
      expect(response.body.message).toBe("Ingredient not found");
    });

    it.each`
      body                            | message                 | reason
      ${undefined}                    | ${/is required/}        | ${"is undefined"}
      ${{}}                           | ${/is required/}        | ${"is empty"}
      ${{ type: IngredientType.END }} | ${/"name" is required/} | ${"has no name field"}
      ${{ name: "name-updated" }}     | ${/"type" is required/} | ${"has no type field"}
    `("Returns 400 if the body $reason", async ({ body, message }) => {
      // given
      const ID = 101;

      mockedIngredient.get.mockResolvedValueOnce({
        id: ID,
        name: "ingredient-name",
        type: IngredientType.START,
      });

      // when
      const response = await request(app).put(`/ingredients/${ID}`).send(body);

      // then
      expect(response.status).toEqual(400);
      expect(response.body).toBeDefined();
      expect(response.body.message).toMatch(message);
    });

    it("Returns 200 and the updated ingredient if the id is valid and exists, and the body is valid", async () => {
      // given
      const ID = 101;
      const body = { name: "name-updated", type: IngredientType.END };

      mockedIngredient.get.mockResolvedValueOnce({
        id: ID,
        name: "ingredient-name",
        type: IngredientType.START,
      });
      mockedIngredient.update.mockResolvedValueOnce(ID);
      mockedIngredient.get.mockResolvedValueOnce({
        id: ID,
        ...body,
      });

      // when
      const response = await request(app).put(`/ingredients/${ID}`).send(body);

      // then
      expect(response.status).toBe(200);
      expect(response.body).toBeDefined();
      expect(response.body).toStrictEqual({
        id: ID,
        ...body,
      });
    });
  });

  describe("DELETE /ingredients/{id}", () => {
    it("Returns 400 if the id is not a number", async () => {
      // given
      const ID = "INVALID";

      // when
      const response = await request(app).delete(`/ingredients/${ID}`);
      // then
      expect(response.status).toEqual(404);
      expect(response.body).toBeDefined();
      expect(response.body.message).toBe("Endpoint not found");
    });

    it("Returns 400 if the ingredient doesn't exist", async () => {
      // given
      const ID = 101;

      mockedIngredient.get.mockResolvedValueOnce(undefined);

      // when
      const response = await request(app).delete(`/ingredients/${ID}`);
      // then
      expect(response.status).toEqual(404);
      expect(response.body).toBeDefined();
      expect(response.body.message).toBe("Ingredient not found");
    });

    it("Returns 204 after deleting the ingredient", async () => {
      // given
      const ID = 101;

      mockedIngredient.get.mockResolvedValueOnce({
        id: ID,
        name: "ingredient-1",
        type: IngredientType.START,
      });
      mockedStep.search.mockResolvedValueOnce([]);
      mockedStep.search.mockResolvedValueOnce([]);
      mockedIngredient.destroy.mockResolvedValueOnce(1);

      // when
      const response = await request(app).delete(`/ingredients/${ID}`);

      // then
      expect(response.status).toBe(204);
      expect(response.body).toStrictEqual({});
    });

    it("Returns 400 if the ingredient is being used as input in a step", async () => {
      // given
      const ID = 101;
      const step = {
        input: ID,
        utensil: 1,
        output: 102,
      };

      mockedIngredient.get.mockResolvedValueOnce({
        id: ID,
        name: "ingredient-1",
        type: IngredientType.START,
      });
      mockedStep.search.mockResolvedValueOnce([step]);
      mockedStep.search.mockResolvedValueOnce([]);

      // when
      const response = await request(app).delete(`/ingredients/${ID}`);

      // then
      expect(response.status).toBe(400);
      expect(response.body).toBeDefined();
      expect(response.body.message).toBe(
        "The ingredient is being used on steps. It can't be deleted."
      );
      expect(response.body.steps).toMatchObject([step]);
    });

    it("Returns 400 if the ingredient is being used as output in a step", async () => {
      // given
      const ID = 102;
      const step = {
        input: 101,
        utensil: 1,
        output: ID,
      };

      mockedIngredient.get.mockResolvedValueOnce({
        id: ID,
        name: "ingredient-2",
        type: IngredientType.END,
      });
      mockedStep.search.mockResolvedValueOnce([]);
      mockedStep.search.mockResolvedValueOnce([step]);

      // when
      const response = await request(app).delete(`/ingredients/${ID}`);

      // then
      expect(response.status).toBe(400);
      expect(response.body).toBeDefined();
      expect(response.body.message).toBe(
        "The ingredient is being used on steps. It can't be deleted."
      );
      expect(response.body.steps).toMatchObject([step]);
    });
  });

  describe("Queries on steps", () => {
    describe("GET /ingredients/{id}/outcomes", () => {
      it("Returns 400 if the id is not a number", async () => {
        // given
        const ID = "INVALID";

        // when
        const response = await request(app).get(`/ingredients/${ID}/outcomes`);
        // then
        expect(response.status).toEqual(404);
        expect(response.body).toBeDefined();
        expect(response.body.message).toBe("Endpoint not found");
      });

      it("Returns 400 if the ingredient doesn't exist", async () => {
        // given
        const ID = 101;

        mockedIngredient.get.mockResolvedValueOnce(undefined);

        // when
        const response = await request(app).get(`/ingredients/${ID}/outcomes`);

        // then
        expect(response.status).toEqual(404);
        expect(response.body).toBeDefined();
        expect(response.body.message).toBe("Ingredient not found");
      });

      it("Returns 400 if the ingredient type is 'end'", async () => {
        // given
        const ID = 101;

        mockedIngredient.get.mockResolvedValueOnce({
          id: ID,
          name: "end",
          type: IngredientType.END,
        });

        // when
        const response = await request(app).get(`/ingredients/${ID}/outcomes`);

        // then
        expect(response.status).toEqual(400);
        expect(response.body).toBeDefined();
        expect(response.body.message).toStrictEqual(
          expect.stringContaining("This is an end ingredient")
        );
      });

      it("Returns 200 and the list of detailed steps with the specified ingredient as input", async () => {
        // given
        const ID = 102;

        mockedIngredient.get.mockResolvedValueOnce({
          id: ID,
          name: "mid",
          type: IngredientType.MID,
        });
        mockedStep.searchDetailed.mockResolvedValueOnce([
          {
            input: {
              id: ID,
              name: "mid",
              type: IngredientType.MID,
            },
            utensil: {
              id: 2,
              name: "utensil-2",
              waitTimeInMillis: 200,
            },
            output: {
              id: 103,
              name: "end-1",
              type: IngredientType.END,
            },
          },
          {
            input: {
              id: ID,
              name: "mid",
              type: IngredientType.MID,
            },
            utensil: {
              id: 3,
              name: "utensil-3",
              waitTimeInMillis: 300,
            },
            output: {
              id: 104,
              name: "end-2",
              type: IngredientType.END,
            },
          },
        ]);

        // when
        const response = await request(app).get(`/ingredients/${ID}/outcomes`);

        // then
        expect(response.status).toBe(200);
        expect(response.body).toBeDefined();
        expect(response.body).toBeInstanceOf(Array);

        response.body.forEach((step: StepType) => {
          expect(step).toMatchObject({
            input: expect.any(Object),
            utensil: expect.any(Object),
            output: expect.any(Object),
          });
          expect(step.input).toMatchObject({ id: ID });
        });
      });
    });

    describe("GET /ingredients/{id}/sources", () => {
      it("Returns 400 if the id is not a number", async () => {
        // given
        const ID = "INVALID";

        // when
        const response = await request(app).get(`/ingredients/${ID}/sources`);
        // then
        expect(response.status).toEqual(404);
        expect(response.body).toBeDefined();
        expect(response.body.message).toBe("Endpoint not found");
      });

      it("Returns 400 if the ingredient doesn't exist", async () => {
        // given
        const ID = 101;

        mockedIngredient.get.mockResolvedValueOnce(undefined);

        // when
        const response = await request(app).get(`/ingredients/${ID}/sources`);

        // then
        expect(response.status).toEqual(404);
        expect(response.body).toBeDefined();
        expect(response.body.message).toBe("Ingredient not found");
      });

      it("Returns 400 if the ingredient type is 'start'", async () => {
        // given

        const ID = 101;

        mockedIngredient.get.mockResolvedValueOnce({
          id: ID,
          name: "start",
          type: IngredientType.START,
        });

        // when
        const response = await request(app).get(`/ingredients/${ID}/sources`);

        // then
        expect(response.status).toEqual(400);
        expect(response.body).toBeDefined();
        expect(response.body.message).toStrictEqual(
          expect.stringContaining("This is a start ingredient")
        );
      });

      it("Returns 200 and the list of detailed steps with the specified ingredient as output", async () => {
        // given
        const ID = 102;

        mockedIngredient.get.mockResolvedValueOnce({
          id: ID,
          name: "mid",
          type: IngredientType.MID,
        });
        mockedStep.searchDetailed.mockResolvedValueOnce([
          {
            input: {
              id: 101,
              name: "start",
              type: IngredientType.START,
            },
            utensil: {
              id: 1,
              name: "utensil-1",
              waitTimeInMillis: 100,
            },
            output: {
              id: ID,
              name: "mid",
              type: IngredientType.MID,
            },
          },
        ]);

        // when
        const response = await request(app).get(`/ingredients/${ID}/sources`);

        // then
        expect(response.status).toBe(200);
        expect(response.body).toBeDefined();
        expect(response.body).toBeInstanceOf(Array);

        response.body.forEach((step: StepType) => {
          expect(step).toMatchObject({
            input: expect.any(Object),
            utensil: expect.any(Object),
            output: expect.any(Object),
          });
          expect(step.output).toMatchObject({ id: ID });
        });
      });
    });
  });
});
